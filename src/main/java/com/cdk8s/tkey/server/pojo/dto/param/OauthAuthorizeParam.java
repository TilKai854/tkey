package com.cdk8s.tkey.server.pojo.dto.param;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


/**
 * 授权参数封装
 */
@Setter
@Getter
@ToString
public class OauthAuthorizeParam {

	/**
	 * 响应类型
	 *  如GlobalVariable.OAUTH_TOKEN_RESPONSE_TYPE代表授权码模式
	 */
	private String responseType;

	/**
	 * 客户端ID
	 */
	private String clientId;

	/**
	 * 重定向URL
	 */
	private String redirectUri;

	/**
	 * 状态
	 */
	private String state;
}
